<?php
/**
 * @package         AnimalsSlideshow
 * @author             Thomas Wenzel
 * @license           GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
?>

        <div id="animalsSlideshow">
        
        <?php 
            foreach($datasets AS $dataset)
            {
        ?>
        
            <div class="animalsSlideshowListCell">
                <div class="imageBox">
                    <img src="<?php echo $dataset['main_picture']; ?>" alt="<?php echo $dataset['name']; ?>" width="140px"/>
                </div><!--#imageBox-->
                <div class="textBox">
                    <ul>
                                <li class="name"><h1><?php echo $dataset['name']; ?></h1></li>
                                <li class="art"><?php echo $dataset['subspecies']; ?></li>
                                <li class="farbe"><?php echo $dataset['color']; ?></li>
                                <li class="geschlecht"><?php echo $dataset['sex']; ?></li>
                                <li class="geburtsdatum">Alter/Geburtsdatum: <?php echo $dataset['birthday']; ?></li>
                                <li class="detail_link">
                                <?php /* Link zur Detailansicht */
                                    $link = JRoute::_("index.php?option=com_animals&view=animal&id=" .$dataset['id']);
                                    echo '<a href="' .$link .'">Ausführliche Beschreibung</a>';
                                ?>
                                </li>
                    </ul>
                </div><!--#textBox-->
            </div><!--#animalsSlideshowListCell-->
            
        <?php
            }
        ?>
                    
        </div><!--#animalsSlideshow-->