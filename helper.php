<?php
/**
 * @package         AnimalsSlideshow
 * @author             Thomas Wenzel
 * @license           GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.model');

class modAnimalsSlideshow
{
    public static function getRandomDatasets($params)
    {
        // Parameter einlesen
        $number = 2;

        // Verbindung zur Datenbank herstellen
        $db		= JFactory::getDbo();

        /* Neues Datenbankabfrage-Objekt besorgen  */
        $query = $db->getQuery(true);

        /* Datenbankabfrage aufbauen */
        
        $query = "SELECT *".
                         "FROM #__animals ".
                         "WHERE (state = 0 OR state = 1)".
                         "ORDER BY RAND()";

        /* Datenbankabfrage an die Datenbank übergeben, Limit wird hier auch übergeben*/
        $db->setQuery($query,0, $number);

        // Die einzelnen Zeilen werden augelesen und übergeben
        $datasets = $db->loadAssocList();

        return $datasets;
    }
}