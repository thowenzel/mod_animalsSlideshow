<?php
/**
 * @package         AnimalsSlideshow
 * @author             Thomas Wenzel
 * @license           GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// Fix for Joomla 3
if(!defined('DS'))
	define('DS',DIRECTORY_SEPARATOR);

// Include the syndicate functions only once
require_once dirname(__FILE__).DS.'helper.php';

/* Import CSS */
$base = JURI::base(true).'/media/com_animals/';
$doc = JFactory::getDocument();
$doc->addStyleSheet($base.'css/animalsdb.css');

$datasets = modAnimalsSlideshow::getRandomDatasets(2);

require(JModuleHelper::getLayoutPath('mod_animalsSlideshow', 'default'));